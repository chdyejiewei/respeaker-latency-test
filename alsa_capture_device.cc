#include "alsa_capture_device.h"


CaptureDevice::CaptureDevice(std::string name, int rate, int channels, snd_pcm_format_t format):
_rate(rate),_device_name(name),_channels(channels),_format(format)
{
    

}

std::string CaptureDevice::GetData(int block_len_ms) 
{
    std::string output;
    _capture_sample_len_per_chl = _rate / 1000 * block_len_ms;
    output.resize(_capture_sample_len_per_chl * _channels * (snd_pcm_format_width(_format) / 8));
    void* output_ptr = (void*)output.data();

    ssize_t r = snd_pcm_readi(_handle, output_ptr, _capture_sample_len_per_chl);
    if (r == -EAGAIN)
    {
        std::cout <<  "Wait for a PCM device to become ready" << std::endl;
        snd_pcm_wait(_handle, 100);
    }
    else if (r == -EPIPE)
    {
        std::cout <<  "Overrun Error" << std::endl;
    }
    else if (r < 0)
    {
        std::cout <<  "Read Error: " << snd_strerror(r) << std::endl;
    }
    else if (r < _capture_sample_len_per_chl)
    {
        std::cout << "Read lost: " << (_capture_sample_len_per_chl - r) << std::endl;
    }

    return output;
}

bool CaptureDevice::StartCapture() 
{
    int err;
    unsigned int new_rate = _rate;
    unsigned int buffer_time = 0, period_time = 0;

    snd_pcm_hw_params_alloca(&_hw_params);

    // blocking mode
    if ((err = snd_pcm_open(&_handle, _device_name.data(), SND_PCM_STREAM_CAPTURE, 0)) < 0) 
    {
        std::cout << "Unable to open PCM device: " << snd_strerror(err) << std::endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_any(_handle, _hw_params)) < 0)
    {
        std::cout <<  "Cannot initialize hardware parameter structure " << snd_strerror(err) << std::endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_set_access(_handle, _hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
    {
        std::cout <<  "Cannot set access type " << snd_strerror(err) << std::endl;
        return false;
    }

    // format:
    if ((err = snd_pcm_hw_params_set_format(_handle, _hw_params, _format)) < 0)
    {
        std::cout <<  "Cannot set format " << snd_strerror(err) << std::endl;
        return false;
    }

    // new_rate: _rate
    if ((err = snd_pcm_hw_params_set_rate_near(_handle, _hw_params, &new_rate, 0)) < 0)
    {
        std::cout <<  "Cannot set rate " << snd_strerror(err) << std::endl;
        return false;
    }
    if (new_rate != _rate)
    {
        std::cout <<  "Sample rate not support " << _rate << std::endl;
        return false;
    }

    // channels: 
    if ((err = snd_pcm_hw_params_set_channels(_handle, _hw_params, _channels)) < 0)
    {
        std::cout <<  "Cannot set channels " << snd_strerror(err) << std::endl;
        return false;
    }

    // Get buffer_time_max, and set buffer time near
    // Buffer time max = 65536000
    if ((err = snd_pcm_hw_params_get_buffer_time_max(_hw_params, &buffer_time, 0)) < 0)
    {
        std::cout <<  "Cannot get buffer time " << snd_strerror(err) << std::endl;
        return false;
    }
    std::cout <<  "Buffer time max is " << buffer_time << std::endl;

    if (buffer_time > 640000)
    {
        buffer_time = 640000;
    }
    else if (buffer_time > 320000)
    {
        buffer_time = 320000;
    }

    if (buffer_time > 0)
    {
        // set Buffer time
        if ((err = snd_pcm_hw_params_set_buffer_time_near(_handle, _hw_params, &buffer_time, 0)) < 0)
        {
            std::cout <<  "Cannot set Buffer time " << snd_strerror(err) << std::endl;
            return false;
        }
        // set Period time, related to chunk size
        // period_time = buffer_time / 4;   // then, chunk_size is 2000, too large
        // period_time = buffer_time / 16;    // 48K chunk_size = 1500
        // period_time = buffer_time / 48;    // 48K chunk_size = 500
        period_time = 8000; // 8ms   
        if ((err = snd_pcm_hw_params_set_period_time_near(_handle, _hw_params, &period_time, 0)) < 0)
        {
            std::cout <<  "Cannot set Period time " << snd_strerror(err) << std::endl;
            return false;
        }
    }

    // save hardware params
    if ((err = snd_pcm_hw_params(_handle, _hw_params)) < 0)
    {
        std::cout <<  "Cannot set params " << snd_strerror(err) << std::endl;
        return false;
    }

    // Get chunk size
    snd_pcm_uframes_t chunk_size = 0;
    snd_pcm_hw_params_get_period_size(_hw_params, &chunk_size, 0);
    std::cout << "Chunk size is " << chunk_size << std::endl;

    // Finish setting Alsa
    std::cout << "Finish Alsa Setting." << std::endl;
    return true;
}

void CaptureDevice::CleanUp()
{
    if (_handle) snd_pcm_close(_handle);
    std::cout << "Alsa Clean Up done." << std::endl;
}
