//

#include <alsa/asoundlib.h>
#include <cstddef>
#include <cstdint>
#include <chrono>
#include <thread>
#include <iostream>

#ifndef __ALSA_CAPTURE_DEVICE__
#define __ALSA_CAPTURE_DEVICE__

class CaptureDevice
{
public:
    CaptureDevice(std::string name, int rate, int channels, snd_pcm_format_t format);
    virtual ~CaptureDevice() = default;

    bool StartCapture();
    void CleanUp();
    std::string GetData(int block_len_ms);

private:
    int _rate = 16000, _channels = 1;
    std::string _device_name = "default";
    snd_pcm_format_t _format = SND_PCM_FORMAT_S16_LE;
    snd_pcm_hw_params_t *_hw_params;
    snd_pcm_t *_handle;

    int _capture_sample_len_per_chl;

};



#endif // !__ALSA_CAPTURE_DEVICE__