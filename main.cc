
#include <cstring>
#include <memory>
#include <iostream>
#include <csignal>
#include <chrono>
#include <thread>

#include "alsa_capture_device.h"


extern "C"
{
#include <sndfile.h>
#include <unistd.h>
#include <getopt.h>
#include <wiringPi.h>
}


using namespace std;

static bool stop = false;


void SignalHandler(int signal){
  cerr << "Caught signal " << signal << ", terminating..." << endl;
  stop = true;
  //maintain the main thread untill the worker thread released its resource
  //std::this_thread::sleep_for(std::chrono::seconds(1));
}

static void help(const char *argv0) {
    cout << "latency_test1 [options]" << endl;
    cout << "A latency test example of snips-respeakerd." << endl << endl;
    cout << "  -h, --help                               Show this help" << endl;
    cout << "  -s, --source=SOURCE_NAME                 The source (microphone) to connect to" << endl;
}


int main(int argc, char *argv[]) {

    // Configures signal handling.
    struct sigaction sig_int_handler;
    sig_int_handler.sa_handler = SignalHandler;
    sigemptyset(&sig_int_handler.sa_mask);
    sig_int_handler.sa_flags = 0;
    sigaction(SIGINT, &sig_int_handler, NULL);
    sigaction(SIGTERM, &sig_int_handler, NULL);

    // parse opts
    int c;
    string source = "default";

    static const struct option long_options[] = {
        {"help",         0, NULL, 'h'},
        {"source",       1, NULL, 's'},
        {NULL,           0, NULL,  0}
    };

    while ((c = getopt_long(argc, argv, "s:h", long_options, NULL)) != -1) {

        switch (c) {
        case 'h' :
            help(argv[0]);
            return 0;
        case 's':
            source = string(optarg);
            break;
        default:
            return 0;
        }
    }

    string data;
    int frames;
    int num_channels = 1;
    int rate = 16000;

    //unique_ptr<CaptureDevice> record;
    
    //record.reset(CaptureDevice(source, rate, num_channels, SND_PCM_FORMAT_S16_LE));
    CaptureDevice record(source, rate, num_channels, SND_PCM_FORMAT_S16_LE);
    if (!record.StartCapture()) {
        cout << "Can not start capture." << endl;
        record.CleanUp();
        return -1;
    }

    cout << "num channels: " << num_channels << ", rate: " << rate << endl;

    // init libsndfile
    SNDFILE	*file ;
    SF_INFO	sfinfo ;

    memset (&sfinfo, 0, sizeof (sfinfo)) ;
    sfinfo.samplerate	= rate ;
	sfinfo.channels		= num_channels ;
	sfinfo.format		= (SF_FORMAT_WAV | SF_FORMAT_PCM_16) ;
    if (! (file = sf_open ("latency_test1.wav", SFM_WRITE, &sfinfo)))
	{
        cout << "Error : Not able to open output file." << endl;
		return -1 ;
	}

    int count = 0;
    int16_t zeros[128*8*2] = {0,};
    memset(&zeros, 32000, 128*8*2);

    wiringPiSetup();
    pinMode(26, OUTPUT);

    
    while (!stop)
    {
        data = record.GetData(8);
        count ++;
        frames = data.length() / (sizeof(int16_t) * num_channels);
        if (count == 100) {
            // speaker play
            digitalWrite(26,HIGH);
            // data set to all zero
            sf_writef_short(file, (const int16_t *)(&zeros), frames);
            continue;
        }
        else if (count == 120) {
            // speaker stop
            digitalWrite(26,LOW);
            // count set to 0
            count = 0;
        }
 
        sf_writef_short(file, (const int16_t *)(data.data()), frames);
        cout << "." << flush;
    }


    record.CleanUp();
    sf_close (file);

    return 0;
}
