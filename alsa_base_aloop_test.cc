/**
 * Copyright (c) 2018 Seeed Technology Co., Ltd.
 *
 * @author JerryYip <chdyejiewei@gmail.com>
 *
 * To compile this file:
 * 
 *      g++ alsa_base_aloop_test.cc -o alsa_base_aloop_test -lrespeaker -lsndfile -fPIC -std=c++11 -fpermissive -I/usr/include/respeaker/ -DWEBRTC_LINUX -DWEBRTC_POSIX -DWEBRTC_NS_FLOAT -DWEBRTC_APM_DEBUG_DUMP=0 -DWEBRTC_INTELLIGIBILITY_ENHANCER=0
 * 
 */

#include <cstring>
#include <memory>
#include <iostream>
#include <csignal>
#include <chrono>
#include <thread>

#include "respeaker.h"
#include "chain_nodes/alsa_collector_node.h"
#include "chain_nodes/selector_node.h"
#include "chain_nodes/aloop_output_node.h"

extern "C"
{
#include <sndfile.h>
#include <unistd.h>
#include <getopt.h>
}


using namespace std;
using namespace respeaker;

static bool stop = false;


void SignalHandler(int signal){
  cerr << "Caught signal " << signal << ", terminating..." << endl;
  stop = true;
  //maintain the main thread untill the worker thread released its resource
  //std::this_thread::sleep_for(std::chrono::seconds(1));
}

static void help(const char *argv0) {
    cout << "alsa_base_aloop_test [options]" << endl;
    cout << "A demo application for librespeaker." << endl << endl;
    cout << "  -h, --help                               Show this help" << endl;
    cout << "  -s, --source=SOURCE_NAME                 The source (microphone) to connect to" << endl;
}


int main(int argc, char *argv[]) {

    // Configures signal handling.
    struct sigaction sig_int_handler;
    sig_int_handler.sa_handler = SignalHandler;
    sigemptyset(&sig_int_handler.sa_mask);
    sig_int_handler.sa_flags = 0;
    sigaction(SIGINT, &sig_int_handler, NULL);
    sigaction(SIGTERM, &sig_int_handler, NULL);

    // parse opts
    int c;
    string source = "default";

    static const struct option long_options[] = {
        {"help",         0, NULL, 'h'},
        {"source",       1, NULL, 's'},
        {NULL,           0, NULL,  0}
    };

    while ((c = getopt_long(argc, argv, "s:h", long_options, NULL)) != -1) {

        switch (c) {
        case 'h' :
            help(argv[0]);
            return 0;
        case 's':
            source = string(optarg);
            break;
        default:
            return 0;
        }
    }


    unique_ptr<AlsaCollectorNode> collector;
    unique_ptr<SelectorNode> selector;
    unique_ptr<AloopOutputNode> aloop;
    unique_ptr<ReSpeaker> respeaker;

    collector.reset(AlsaCollectorNode::Create(source, 48000, 8, false));
    std::vector<int> selected_channels {0,};
    // selector node output interleaved data and store into wav file
    selector.reset(SelectorNode::Create(selected_channels, true));
    aloop.reset(AloopOutputNode::Create("hw:Loopback,0,0", true));

    selector->Uplink(collector.get());
    aloop->Uplink(selector.get());

    respeaker.reset(ReSpeaker::Create());
    respeaker->RegisterChainByHead(collector.get());
    respeaker->RegisterOutputNode(aloop.get());

    if (!respeaker->Start(&stop)) {
        cout << "Can not start the respeaker node chain." << endl;
        return -1;
    }

    string data;
    int frames;
    size_t num_channels = respeaker->GetNumOutputChannels();
    int rate = respeaker->GetNumOutputRate(); 

    cout << "num channels: " << num_channels << ", rate: " << rate << endl;

    // init libsndfile
    SNDFILE	*file ;
    SF_INFO	sfinfo ;

    memset (&sfinfo, 0, sizeof (sfinfo)) ;
    sfinfo.samplerate	= rate ;
	sfinfo.channels		= num_channels ;
	sfinfo.format		= (SF_FORMAT_WAV | SF_FORMAT_PCM_16) ;
    if (! (file = sf_open ("alsa_base_aloop_test.wav", SFM_WRITE, &sfinfo)))
	{
        cout << "Error : Not able to open output file." << endl;
		return -1 ;
	}
    sf_count_t n = 0;
    while (!stop)
    {
        data = respeaker->Listen();
        frames = data.length() / (sizeof(int16_t) * num_channels);
        n = sf_writef_short(file, (const int16_t *)(data.data()), frames);
        cout << "." << flush;
    }

    cout << "stopping the respeaker worker thread..." << endl;

    respeaker->Stop();
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    if (sf_close (file) != 0) 
    {
        cout << "sf close error" << endl;
    }

    cout << "cleanup done." << endl;

    
    return 0;
}


