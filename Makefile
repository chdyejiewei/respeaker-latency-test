all : latency_test1 blink
.PHONY : clean

latency_test1: main.cc alsa_capture_device.o
	g++ main.cc alsa_capture_device.o -o latency_test1 -lasound -lsndfile -lwiringPi -std=c++11 -fpermissive

alsa_capture_device.o : alsa_capture_device.cc
	g++ -c alsa_capture_device.cc -std=c++11

blink: blink.c
	gcc blink.c -o blink -lwiringPi

clean:
	rm *.o blink main
